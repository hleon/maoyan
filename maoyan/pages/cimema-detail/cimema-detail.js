// pages/cimema-detail/cimema-detail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cimamaDetail:{},
    divideDealList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      var cimemaId = options.cimemaId;
      this.initPage(cimemaId);
  },
  initPage(cimemaId){
    console.log(cimemaId);

    wx.request({
      url: 'https://m.maoyan.com/ajax/cinemaDetail?cinemaId='+cimemaId+'&optimus_uuid=795BE890ABDD11EAA754FBD44929A0D4806DC89953A54FFCAD7F1E7BD9388D8B&optimus_risk_level=71&optimus_code=10',
      success:(res)=>{

        console.log(res);
        var divideDealList = this.data.divideDealList;
        divideDealList = res.data.dealList.activity;
        this.setData({
          cimamaDetail:res.data,
          divideDealList:divideDealList
        })
      },
    })
  
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})