var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 想看的带电影
    wantSeeList : app.globalData.wantSeeList,
    hotShowList:[],
    newShowList:[],
    mostExpectedList:[],
    moreClassicList:[],
    isShouchang:false,
    active: 1,
    // 搜素标签
    searchTagActive: 0,
    searchTag:"<van-icon name='search' />"
  },
  // 添加想看
  add(e){
   
     console.log(e);
    var movieId = e.currentTarget.dataset.id;
    console.log(movieId);
    var newShowList = this.data.newShowList;
    for(var i = 0;i <newShowList.length;i++ ){
      if(movieId == newShowList[i].id ){
        console.log(newShowList[i]);
        newShowList[i].wantSee = !newShowList[i].wantSee;
        if(newShowList[i].wantSee){
          this.data.wantSeeList.push(newShowList[i]);
          this.setData({
            wantSeeList:this.data.wantSeeList
          })
          wx.showToast({
            title: '已标记想看',
            icon: 'success_no_circle',
          });
        }else{
          wx.showToast({
            title: '已取消标记',
            icon: 'none'
          });
        }
      }
    }
    this.setData({
      newShowList:newShowList,
     
    })
    app.globalData.wantSeeList = this.data.wantSeeList;

    console.log("app.globalData.wantSeeList");
    console.log(app.globalData.wantSeeList);
    // console.log(this.data.newShowList)
  },
  // onChange(event) {
  //   wx.showToast({
  //     title: `切换到标签 ${event.detail.name}`,
  //     icon: 'none',
  //   });
  // },
  // 获取热门
  movieOnInfoList(){
    wx.request({
      url: 'http://www.h5yjy.com/api/movieOnInfoList?cityId='+10,
      success:(res)=>{
        var movieList = this.data.hotShowList;
        movieList = res.data.data.movieList;
        movieList.map((item)=>{
          item.img = item.img.replace("w.h","66.94");

        })
  
        this.setData({hotShowList:movieList});
      }
    })
  },

  // 获取近期
  newMovieOnInfoList(){
    wx.request({
      url: 'http://39.97.33.178/api/movieComingList?cityId='+10,
      success:(res)=>{
        var movieList = this.data.newShowList;
        movieList = res.data.data.comingList;
        movieList.map((item)=>{
          item.img = item.img.replace("w.h","66.94");
          console.log(item.img);
        })
        console.log(movieList);
        this.setData({newShowList:movieList});
        console.log(this.data.newShowList)
      }
    })
  },

  // 获取近期最受期待
  // https://m.maoyan.com/ajax/mostExpected?ci=40&limit=10&offset=0&token=&optimus_uuid=795BE890ABDD11EAA754FBD44929A0D4806DC89953A54FFCAD7F1E7BD9388D8B&optimus_risk_level=71&optimus_code=10
  moreClassicList(){
    wx.request({
      url: 'https://m.maoyan.com/ajax/moreClassicList?sortId=1&showType=3&limit=10&offset=10&optimus_uuid=795BE890ABDD11EAA754FBD44929A0D4806DC89953A54FFCAD7F1E7BD9388D8B&optimus_risk_level=71&optimus_code=10',
      success:(res)=>{
        console.log(res)
        var movieList = this.data.moreClassicList;
        console.log(res)
        movieList = res.data.coming;
     
      }
    })
  },

  // https://m.maoyan.com/ajax/moreComingList?ci=40&token=&limit=10&movieIds=1219866%2C1277751%2C1299124%2C1298859%2C1332663%2C1244901%2C1048268%2C1212627%2C1220824%2C1217023&optimus_uuid=795BE890ABDD11EAA754FBD44929A0D4806DC89953A54FFCAD7F1E7BD9388D8B&optimus_risk_level=71&optimus_code=10
  mostExpectedList(){
    wx.request({
      url: 'https://m.maoyan.com/ajax/mostExpected?ci=40&limit=10&offset=0&token=&optimus_uuid=795BE890ABDD11EAA754FBD44929A0D4806DC89953A54FFCAD7F1E7BD9388D8B&optimus_risk_level=71&optimus_code=10',
      success:(res)=>{
        console.log(res)
        var movieList = this.data.mostExpectedList;
        movieList = res.data.coming;
        movieList.map((item)=>{
          item.img = item.img.replace("w.h","66.94");
          // console.log(item.img);
        })
        // console.log(movieList);
        this.setData({mostExpectedList:movieList});
        console.log(this.data.mostExpectedList)
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.movieOnInfoList();
    this.newMovieOnInfoList();
    this.mostExpectedList();
    this.moreClassicList();
    
  },
 

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    console.log(this.data.newShowList.length);
    for(var i = 0;i <this.data.newShowList.length;i++ ){
      
      this.data.newShowList[i].wantSee = false;
      }
     this.setData({
        newShowList:this.data.newShowList
      })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})