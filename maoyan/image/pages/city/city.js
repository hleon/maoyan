// pages/city/city.js
const app = getApp();
Page({
  data: {
    list: [],
    indexList:[],
  },
  onReady() {
    wx.request({
      url: 'http://www.h5yjy.com/api/cityList',
      success: (res) => {
        var list = [];
        for (var i = 0; i < 26; i++) {
          list.push(String.fromCharCode(97 + i))
        }
        var res = res.data.data.cities;
        list.map((item, index) => { //a-z 26字母  [a,b,c,.....]
          list[index] = {
            k: item,
            p:item.toLocaleUpperCase(),
            list: []
          }
          //[{k:'a',p:'A',list:[]},{k:'b',p:'b',list:[]}]
          res.map((key) => {
            var p = key.py.slice(0, 1);
            if (list[index].k === p) {
              list[index].list.push(key)
            }
          })
        })
        var indexList = this.data.indexList;
        list.map((item,index)=>{
          if(item.list.length===0){
            list.splice(index,1);
          }
          indexList.push(item.p)
        })
        this.setData({list,indexList});
      }
    })
  },
  getCurrentCity(e){
    app.globalData.currentCity=e.currentTarget.dataset.item;
    app.globalData.callBackEvent();
    wx.switchTab({
      url: '/pages/index/index',
    })
  }

})