//index.js
//获取应用实例
let app = getApp()
Page({
    data: {
        cinemas:[],
        city: "天津市",
        items: [{
            "nm": "UA电影城（上海梅龙镇广场店）",
            "price": "14.9",
            "addr": "静安区南京西路1038号梅龙镇广场10楼（近江宁路）",
            "discount": "超凡战队等五部电影特惠",
            "card": "开卡特惠，首单2张最高立减2元",
            "distance": "863.4km",
            "ways": [{
                "w": "座",
                "color": "#589daf"
            }, {
                "w": "退",
                "color": "#589daf"
            }, {
                "w": "改签",
                "color": "#589daf"
            }, {
                "w": "小吃"
            }, {
                "w": "折扣卡"
            }]
        }, {
            "nm": "大地影院（大华乐购店）",
            "price": "14.9",
            "addr": "宝山区大华路518号4楼（华润万家4楼）",
            "ways": [{
                "w": "座",
                "color": "#589daf"
            }, {
                "w": "退",
                "color": "#589daf"
            }, {
                "w": "改签",
                "color": "#589daf"
            }, {
                "w": "小吃",
                "color": "#ff9900"
            }, {
                "w": "折扣卡",
                "color": "#ff9900"
            }],
            "discount": "提着心，吊着胆等5部电影特惠",
            "card": "开卡特惠，首单1张最高立减10元",
            "distance": "858.7km"
        }, {
            "nm": "AMG海上明珠影城（上海莘庄店）",
            "price": "14.9",
            "addr": "闵行区沪闵路6088号莘庄凯德龙之梦购物中心4楼（近莘建路）",
            "ways": [{
                "w": "座",
                "color": "#589daf"
            }, {
                "w": "退",
                "color": "#589daf"
            }, {
                "w": "改签",
                "color": "#589daf"
            }, {
                "w": "小吃",
                "color": "#ff9900"
            }, {
                "w": "折扣卡",
                "color": "#ff9900"
            }],
            "discount": "提着心，吊着胆等5部电影特惠",
            "card": "开卡特惠，首单2张最高立减6元",
            "distance": "876.9km"
        }, {
            "nm": "左岸国际电影城（松江沃尔玛店）",
            "price": "14.9",
            "addr": "松江区松汇中路568号鹿都沃尔玛广场4楼（近庙前街）",
            "ways": [{
                "w": "座",
                "color": "#589daf"
            }, {
                "w": "退",
                "color": "#589daf"
            }, {
                "w": "改签",
                "color": "#589daf"
            }, {
                "w": "小吃",
                "color": "#ff9900"
            }, {
                "w": "折扣卡",
                "color": "#ff9900"
            }],
            "discount": "提着心，吊着胆等5部电影特惠",
            "card": "开卡特惠，首单2张最高立减4元",
            "distance": "888.8km"
        }],
        "ways": [{
            "w": "座",
            "color": "#589daf"
        }, {
            "w": "退",
            "color": "#589daf"
        }, {
            "w": "改签",
            "color": "#589daf"
        }, {
            "w": "小吃",
            "color": "#ff9900"
        }, {
            "w": "折扣卡",
            "color": "#ff9900"
        }],
        desc: []
    },
    // 获取城市信息
    getcinemaList(){

        // 发送Ajax请求获取数据
        wx.request({
          url: 'http://39.97.33.178/api/cinemaList?cityId=10',
          success:(res)=>{
            console.log(res)
            this.data.cinemas = res.data.data.cinemas;
            
            let that = this;
            this.setData({
                cinemas:res.data.data.cinemas
            })
            console.log(this.data.cinemas)
          },
          error:(res)=>{

          }
        })
    },
    toCity(){
        wx.navigateTo({
          url: '../city/city',
        })
      },
      // 去搜索界面
  toSearch(){
    wx.navigateTo({
      url: '/pages/search/search'
    })
  },
    onLoad: function() {
        // 发送请求获取数据
        console.log(this.data.items);
        // wx 是微信的缩写
        let that = this;
    },
    onShow: function() {

        this.getcinemaList();
        console.log('Onshow')
        let that = this;
        wx.getStorage({
            key: 'key',
            success: function(res) {
                console.log(res.data.cardmes.city);
                that.setData({
                    city: res.data.cardmes.city,
                })

            }
        })
    },

})