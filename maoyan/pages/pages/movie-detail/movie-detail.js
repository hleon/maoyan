Page({

  /**
   * 页面的初始数据
   */
  data: {
    detailMovie:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var movieId = options.movieId;
    this.initPage(movieId);
  },

  initPage(movieId){
    var that = this;
    wx.request({
      url: 'http://39.97.33.178/api/detailmovie?movieId='+movieId,
      success(res){
          console.log(res);
           var detailMovie = res.data.data.detailMovie;
  
           detailMovie = res.data.data.detailMovie;
         
           detailMovie.img = detailMovie.img.replace("w.h","66.94");
             console.log(detailMovie.img);
            var photos = [];
            photos = detailMovie.photos;
            console.log(photos);
            detailMovie.photos.map((item,index)=>{
              detailMovie.photos[index] = detailMovie.photos[index].replace("w.h","66.94");
            // console.log(item);
            
          })
          // detailMovie.photos = photos;
          console.log(detailMovie.photos)
           that.setData({
               detailMovie
           })
      }
    })
  },
  toPlayPage(){
    var data = JSON.stringify(this.data.detailMovie)
    wx.navigateTo({
      url: '/pages/play_video/play_video?data='+data,
     
    })
  },


  flex: function () {
      var that = this
      this.setData({
        flexed: !that.data.flexed
      })
    },


  updatePhotos(){

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})