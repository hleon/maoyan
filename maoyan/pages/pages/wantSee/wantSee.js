// 获取全局数据
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 想看的带电影
    wantSeeList : app.globalData.wantSeeList,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      console.log(this.data.wantSeeList)
      console.log(app.globalData.wantSeeList)
      this.data.wantSeeList = app.globalData.wantSeeList;
      this.setData({
        wantSeeList:this.data.wantSeeList
      })
      console.log(this.data.wantSeeList)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})