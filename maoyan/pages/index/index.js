// pages/index/index.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    active: 1,
    hotShowList:[],
    currentCity:[]
  },
  // 滑动效果
  onChange(event) {
    wx.showToast({
      title: `切换到标签 ${event.detail.name}`,
      icon: 'none',
    });
  },

  movieOnInfoList(){
    wx.request({
      url: 'http://www.h5yjy.com/api/movieOnInfoList?cityId='+10,
      success:(res)=>{
        var movieList = this.data.hotShowList;
        movieList = res.data.data.movieList;
        movieList.map((item)=>{
          item.img = item.img.replace("w.h","66.94");
          console.log(item.img);
        })
        console.log(movieList.img);
        this.setData({hotShowList:movieList});
      }
    })
  },
  toMovieDetailPage(e){
    var movieId = e.target.dataset.id;
    
    wx.request({
      url:"http://39.97.33.178/api/detailmovie?movieId='movieId",
      success:(res)=>{
        console.log(res);
      }
    })
    console.log(e)

  },
  getLocation(){
    wx.request({
      url: 'http://www.h5yjy.com/api/getLocation',//获取当前城市
      success:(res)=>{
        var currentCity = res.data.data;// {nm:"北京",id:111}
        // 把值给全局globalcity
        app.globalData.currentCity=currentCity;
        // 根据当前城市id,获取当前城市 数据
        this.movieOnInfoList(currentCity.id)
        this.setData({currentCity});//更新视图的
      }
    })
   },
   toSearch(){
    wx.navigateTo({
      url: '/pages/search/search',
    })
   },
    toCity(){
      wx.navigateTo({
        url: '../city/city',
      })
    },
    toMovieDetailPage(){
      wx.navigateTo({
        url: '../city/city'
      })
    },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      this.movieOnInfoList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
//初始化调用
    //获取当前城市
    this.getLocation();
    app.globalData.callBackEvent = ()=>{
      var currentCity = app.globalData.currentCity;
      this.setData({currentCity})
      this.movieOnInfoList(currentCity.id);
    }
  },
  // 去搜索界面
  toSearch(){
    wx.navigateTo({
      url: '/pages/search/search'
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})