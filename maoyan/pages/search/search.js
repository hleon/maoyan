// pages/search/search.js

var pinyin= require("../../utils/dist/index");
Page({
  /**
   * 页面的初始数据
   */
  data: {
      movieList :[]
  },
  setInput(e){
    let that = this;
    var result =pinyin.convertToPinyin(e.detail);
    wx.request({
      url: 'http://www.h5yjy.com/api/searchList?cityId=10&kw='+result,
      success:(res)=>{
        console.log(res);
         console.log(res.data.data.movies.list);
        //  var movieList = res.data.data.movies.list;
         var movieList = this.data.movieList;
         movieList = res.data.data.movies.list;
         movieList.map((item)=>{
           item.img = item.img.replace("w.h","66.94");
           console.log(item.img);
         })
         that.setData({
           movieList
         })
      }
    })
  }
})